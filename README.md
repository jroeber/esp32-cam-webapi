# ESP32-CAM Web API

A basic program for getting pictures from the ESP32-CAM over the network.

## Instructions

(Assumes basic knowledge of working with ESP32-CAM boards.)

- Add your SSID/password to `src/main.cpp`.
- (Optional) Set a static IP address below your SSID/password in `src/main.cpp`.
- Flash to the ESP32-CAM with PlatformIO.
- Get the IP address from the serial output if you did not set a static IP.
- Run `curl -o test.jpg <IP Address>` to take and save a picture to your local machine.
